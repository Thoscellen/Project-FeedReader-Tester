/// <reference types="Cypress" />

describe('Feedreader homepage', function () {
    it('Visits Feedreader homepage', function() {
        // Act
        cy.visit("");
        // Asset
        cy.url().should("begin", "https://feedreader.com/");
    })

    it('Launch Feedreader online', function() {
        // Arrange
        cy.visit("");
        // Act
        cy.get('a[href="//feedreader.com/online"]').click()
        // Asset
        cy.url().should('contain', 'online')
    })

    it('Launch Feedreader observe', function() {
        // Arrange
        cy.visit("");
        // Act
        cy.get('a[href="https://feedreader.com/observe/"]').click({force : true, multiple : true})
        // Asset
        cy.url().should('contain', 'observe')

    })

})
