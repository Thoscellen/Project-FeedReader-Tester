/// <reference types="Cypress" />

describe('As a visitor, I am able to log in', function () {

    beforeEach('Go to the login webpage.', function () {
        cy.visit("/online");
    })

    it('Ckeck that the login page is accessible', function () {
        // Asset
        cy.url().should('include', '?action=login');
    })

    it('Check that the login tab is active by default', function () {
        // Asset
        cy.get('.menu-reg').first().find('a[href$="action=login"]').should('have.class', 'active');
    })
});

describe('As a visitor, I can only register the right way.', function () {

    beforeEach('go to the login webpage.', function () {
        cy.visit("/online");
    });

    it('Check that the "Create Account" tab is accessible', function () {
        // Assert
        cy.get('.menu-reg').first().find('a[href$="action=register"]').should('be.visible');
    });

    it('Check that the "Create Account" tab shows a form to fill when active', function () {
        // Act : Visiter le formulaire pour s'inscrire.
        cy.get('.menu-reg').find('a[href$="action=register"]').click();
        // Assert
        cy.url().should('include', '?action=register');
        cy.get('.menu-reg').find('a[href$="action=register"]').should('have.class', 'active')       // L'onglet "Create accout" possède la classe "active".
        cy.get('input#register-email').should('be.visible');      // Les formulaires existent et sont visible
        cy.get('input#register-password').should('be.visible');
        cy.get('#register-form').find(":submit");        // Le bouton "Create Account" existe.
    });

    it('Allow to land directly on the Register page', function () {
        //Act
        cy.visit('/online/#/welcome/?action=register')
        //Assert
        cy.get('.menu-reg').first().find('a[href$="action=register"]').should('have.class', 'active');
    })

    it('Prevent to create an account with empty fields', function () {
        // Arrange
        cy.get('.menu-reg').first().find('a[href$="action=register"]').click();
        // Assert everything is fine before acting
        cy.get('input#register-email').should('have.not.class', 'input_error');
        cy.get('input#register-password').should('have.not.class', 'input_error');
        cy.get('div#register-error').should("not.be.visible");
        // Act
        cy.get('#register-form').find(":submit").click();
        // Assert
        cy.get('div#register-error')         // Message d'erreur
            .should('have.class', 'register__error')
            .and('be.visible');
        cy.get('input#register-email').should('have.class', 'input_error');         // Champs surlignés
        cy.get('input#register-password').should('have.class', 'input_error');
    })

    it('Prevent to create an account with an already existing account', function () {
        // Arrange
        cy.fixture("users.json").as("users").then((users) => {
            let mutoid = users["Mutoid"];
            expect(mutoid.email).to.exist;
            expect(mutoid.password).to.exist;
            expect(mutoid.type).eq("user");
            cy.get('.menu-reg').first().find('a[href$="action=register"]').click();

            // Act
            cy.get('input#register-email').type(mutoid.email);
            cy.get('input#register-password').type(mutoid.password);
            cy.get('#register-form').find(":submit").click();

            // Assert
            cy.get('input#register-email').should('have.class', 'input_error');
            cy.get('input#register-password').should('have.class', 'input_error');
            cy.get('div#register-error')
                .should('be.visible')
                .and('contain', 'This email is already registered, please choose another one.')
            //We should find a way to get the response body and assert it is well displayed instead of this string.
        });
    })

    it('Check that it explain correclty to the user when he try to create an already existing account', function () {
        cy.server();
        cy.fixture("users.json").as("users").then((users) => {
            let mutoid = users["Mutoid"];

            //cy.route("online/api/user", "fx:/errors/registerExistingUser.json");
            cy.request({
                method: 'POST',
                url: "online/api/user",
                body: mutoid,
                failOnStatusCode: false,
                qs: { "t": new Date().getTime() }
            }).then((response) => {
                cy.log(response);
                cy.expect(response.status).to.be.eq(400);
                cy.expect(response.body.status).to.be.eq('error');
                cy.expect(response.body.errors[0]).to.be.eq('This email is already registered, please choose another one.');
            });

        });
    })

});

describe("As a user, I can only log in and out the right way", function () {

    var mutoid;

    before(function () {
        cy.fixture("users.json").as("users").then((users) => {
            mutoid = users["Mutoid"];
        });
    })

    beforeEach(function () {
        cy.visit("/online");
    })

    it('Prevent to log in with empty credentials', function () {
        // Act
        cy.get('form#login-form').find(':submit').click();
        // Assert
        cy.url().should('include', '?action=login');  // Pas de redirection
        cy.get('div#login-error')
            .should('have.class', 'login__error')
            .and('be.visible')
            .and('contain', "Please fill the email and password fields"); // Message d'erreur de l'API
        // Champs éclairés
        cy.get('input#login-email').should('have.class', 'input_error');
        cy.get('input#login-password').should('have.class', 'input_error');
    })

    it('Prevent to log in with just the email', function () {
        // Act
        cy.get("#login-email").type(mutoid.email);
        cy.get('form#login-form').find(':submit').click();
        // Assert
        cy.url().should('include', '?action=login');  // Pas de redirection
        cy.get('div#login-error')
            .should('have.class', 'login__error')
            .and('be.visible'); // Message d'erreur
        // Champs éclairés
        cy.get('input#login-email').should('have.class', 'input_error');
        cy.get('input#login-password').should('have.class', 'input_error');
    })

    it('Prevent to log in with just the password', function () {
        // Act
        cy.get("#login-password").type(mutoid.password)
        cy.get('form#login-form').find(':submit').click();
        // Assert
        cy.url().should('include', '?action=login');  // Pas de redirection
        cy.get('div#login-error')
            .should('have.class', 'login__error')
            .and('be.visible'); // Message d'erreur
        // Champs éclairés
        cy.get('input#login-email').should('have.class', 'input_error');
        cy.get('input#login-password').should('have.class', 'input_error');
    })

    it('Prevent to log in mutoid with an invalid password', function () {
        // Act
        cy.get("#login-email").type(mutoid.email)
        cy.get("#login-password").type(mutoid.email)
        cy.get('form#login-form').find(':submit').click();
        // Assert
        cy.url().should('include', '?action=login');  // Pas de redirection
        cy.get('div#login-error')
            .should('have.class', 'login__error')
            .and('be.visible'); // Message d'erreur
        // Champs éclairés
        cy.get('input#login-email').should('have.class', 'input_error');
        cy.get('input#login-password').should('have.class', 'input_error');
    })

    it('Logs Mutoid in.', function () {
        // Act
        cy.get("#login-email").type(mutoid.email)
        cy.get("#login-password").type(mutoid.password)
        cy.get("#login-form").find(":submit").click()
        // Assert
        cy.url().should('contain', '/online/#/reader/category/all/')
        cy.get("div#user").find("a.user__settings").click();
        cy.getCookie('session').should('exist');
    });

    it('Logs Mutoid in via API', function () {
        // act
        cy.login(mutoid);
        cy.visit("/online");
        // assert
        cy.url().should('contain', '/online/#/reader/category/all/')
        cy.get("div#user").find("a.user__settings").click();
        cy.getCookie('session').should('exist');
    });

    it('Logs Mutoid out.', function () {
        // Arrange
        cy.login(mutoid)
        cy.visit('/online');
        cy.getCookie('session').should('exist');
        // act
        cy.get("div#user").find("a.user__settings").click();
        cy.get("div#user-menu").find('.logout').click();
        //assert
        cy.url().should('include', '?action=login');
        cy.get('.menu-reg').first().find('a[href$="action=login"]')
            .should('have.class', 'active');
        cy.getCookie('session').should('not.exist');
    })
})